<?php

/**
 * Created by PhpStorm.
 * User: Андрей
 * Date: 16.11.2017
 * Time: 16:20
 */
class connect// extends  PDO
{
    private $conect;
    public function __construct($file = 'php.ini')
    {
        if (!$settings = parse_ini_file($file, TRUE)) throw new exception('Unable to open ' . $file . '.');

        $dns = $settings['database']['driver'] .
            ':host=' . $settings['database']['host'] .
            ((!empty($settings['database']['port'])) ? (';port=' . $settings['database']['port']) : '') .
            ';dbname=' . $settings['database']['schema'];

        $this->conect =new PDO($dns, $settings['database']['username'], $settings['database']['password'], $settings['db_options']);
    }

    public  function  showAll()
    {
        $sql = "SELECT *
                      FROM Products AS p
                      LEFT JOIN Categories AS c ON  c.id=p.id_cat
                      ORDER BY p.name_prod";
        $q = $this->conect->query($sql) or die("Failed to do query for show all categories with the appropriate products!");
        while($req = $q->fetch(PDO::FETCH_ASSOC)){
            $data[]=$req;
        }
        return $data;
    }

    public function showProd($id)
    {
        $sql = "SELECT * FROM Products WHERE id_cat = $id";
        $q =  $this->conect->query($sql) or die("Failed to do query for list of products!");
        while($req = $q->fetch(PDO::FETCH_ASSOC)){
            $data[]=$req;
        }
        return $data;
    }

    public function add($table, $named, $id_c = null)
    {
        if(!empty($id_c))// we have prod with id category
        {
            $sql  = "INSERT INTO $table SET name_prod=:named, id_cat=:id_c";
            $q =  $this->conect->prepare($sql);
            $q->execute(array(':named'=>$named,':id_c'=>$id_c));
        }
        else //we have new category
        {
            $sql = "INSERT INTO $table SET name_cat=$named";
            $q =   $this->conect->prepare($sql);
            $q->execute(array(':named'=>$named));
        }
        return true;
    }

    public function edit($table, $id, $named,$id_c = null)
    {
        if(!empty($id_c))
        {
            $sql = "UPDATE $table SET name_prod=:named, id_cat=:id_c WHERE id=:id";
            $q =  $this->conect->prepare($sql);
            $q->execute(array(':id'=>$id, ':named'=>$named,':id_c'=>$id_c));
        }
        else {
            $sql = "UPDATE INTO $table SET name_cat=$named WHERE id=:id";
            $q =  $this->conect->prepare($sql);
            $q->execute(array(':id' => $id, ':named' => $named));
        }
        return true;
    }

    public function delete($table, $id)
    {
        $sql = "DELETE FROM $table WHERE id=:id";
        $q =  $this->conect->prepare($sql);
        $q->execute(array(':id'=>$id));
        return true;
    }
}